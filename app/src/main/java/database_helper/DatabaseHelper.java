package database_helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import process.CopyDB;
import utils.Constants;
import utils.RoutesVariables;

import static utils.Constants.DB_LOCATION;
import static utils.Constants.DB_NAME;
import static utils.Constants.TABLE_NAME_ROUTES;

/**
 * Created by jehuBasa on 17/07/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context mContext;
    public SQLiteDatabase mDB;
    public Cursor cursor;

    public DatabaseHelper(Context c) {
        super(c, DB_NAME, null, 1);
        mContext = c;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        CopyDB n = new CopyDB(mContext);
        if (i1 > i) {
            n.execute();
        }
    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DB_NAME).getPath();
        if (mDB != null && mDB.isOpen()) {
            return;
        } else {
            mDB = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READONLY);
        }

    }

    public void closeDatabase() {
        if (mDB != null) {
            mDB.close();
        }
    }

    public static boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_LOCATION + DB_NAME;
            File file = new File(myPath);
            if (file.exists() && !file.isDirectory())
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            //database does't exist yet.
        }

        if (checkDB != null) {
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }


    public ArrayList<String> getRoutes() {
        ArrayList<String> mRoutes = new ArrayList<>();
        openDatabase();
        cursor = mDB.rawQuery("SELECT * FROM " + TABLE_NAME_ROUTES, null);
        cursor.moveToFirst();
        boolean isFirst = true;
        while (!cursor.isAfterLast()) {
            if(isFirst){
                mRoutes.add(0, cursor.getString(1));
                isFirst = false;
            }else {
                mRoutes.add(cursor.getString(1));
            }
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return mRoutes;
    }
}
