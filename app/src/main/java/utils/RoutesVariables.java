package utils;

/**
 * Created by jehuBasa on 18/07/2017.
 */

public class RoutesVariables {
    int route_id;
    String route_name;

    public RoutesVariables(int route_id, String route_name) {
        this.route_id = route_id;
        this.route_name = route_name;
    }
}
