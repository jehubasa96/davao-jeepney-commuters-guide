package utils;

/**
 * Created by jehuBasa on 17/07/2017.
 */

public class Constants {

    //Strings
    public static final String DB_LOCATION = "/data/data/com.code.jehubasa.davaojeepneycommuterguide/databases/";

    //For DATABASE
    public static final String DB_NAME = "jeepney";
    public static final String TABLE_NAME_DIRECTIONS = "directions";
    public static final String TABLE_NAME_POINTS = "points";
    public static final String TABLE_NAME_ROUTES = "routes";
}
