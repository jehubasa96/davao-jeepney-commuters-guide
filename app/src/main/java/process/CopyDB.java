package process;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.code.jehubasa.davaojeepneycommuterguide.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import database_helper.DatabaseHelper;
import utils.Constants;

import static utils.Constants.DB_LOCATION;
import static utils.Constants.DB_NAME;


public class CopyDB extends AsyncTask<Void, Void, Boolean> {

    public static final String TAG = "CopyDB";

    private Context c;
    private ProgressDialog mPdialog;

    public CopyDB(){}

    public CopyDB (Context c){this.c = c;}

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mPdialog = ProgressDialog.show(c, "Working", c.getString(R.string.copying_database));
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        File dir = new File(Constants.DB_LOCATION);
        dir.mkdir();

        if (!DatabaseHelper.checkDataBase()) {
            try {
                InputStream input = c.getAssets().open(DB_NAME);
                String outFileName = DB_LOCATION + DB_NAME;
                OutputStream output = new FileOutputStream(outFileName);
                byte[] buf = new byte[1024];
                int length = 0;

                while ((length = input.read(buf)) > 0){
                    output.write(buf, 0, length);
                }

                output.flush();
                output.close();
                Log.d(TAG, "Copying of DB is successful.");
                return true;

            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Copying of DB is not successful.");
                return false;
            }
        } else{
            Log.d(TAG, "Opps! DB is already existing");
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        mPdialog.dismiss();
        if (aBoolean){
            Toast.makeText(c, "Successful in creating database", Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG,"Done");
    }
}
